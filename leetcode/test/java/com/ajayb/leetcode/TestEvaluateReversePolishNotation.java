package com.ajayb.leetcode;

import org.testng.annotations.Test;

import java.util.ArrayList;

/**
 * To change this template use File | Settings | File Templates.
 */
public class TestEvaluateReversePolishNotation {

    @Test
    public void evaluateReversePolishNotation() throws Exception {

        EvaluateReversePolishNotation evaluateReversePolishNotation = new EvaluateReversePolishNotation();
        ArrayList<String> tokenList = new ArrayList<String>();
        tokenList.add("2");
        tokenList.add("1");
        tokenList.add("+");
        tokenList.add("3");
        tokenList.add("*");
        int result = evaluateReversePolishNotation.evalRPN(tokenList.toArray(new String[]{}));
        System.out.println("result = " + result);

    }
}
