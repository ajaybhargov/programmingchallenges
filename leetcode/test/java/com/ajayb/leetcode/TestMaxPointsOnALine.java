package com.ajayb.leetcode;

import org.testng.annotations.Test;

import java.util.ArrayList;

/**
 * To change this template use File | Settings | File Templates.
 */
public class TestMaxPointsOnALine {

    @Test
    public void testMaxPointsOnALine() throws Exception {
        MaxPointsOnALine maxPointsOnALine = new MaxPointsOnALine();
        ArrayList<Point> points = new ArrayList<Point>();
        points.add(new Point());

        int maxPoints = maxPointsOnALine.maxPoints(points.toArray(new Point[0]));
        System.out.println("maxPoints = " + maxPoints);
    }

    @Test
    public void testDiagonalPoints() throws Exception {
        MaxPointsOnALine maxPointsOnALine = new MaxPointsOnALine();
        ArrayList<Point> points = new ArrayList<Point>();
        points.add(new Point(0, 0));
        points.add(new Point(-1, -1));
        points.add(new Point(2, 2));

        int maxPoints = maxPointsOnALine.maxPoints(points.toArray(new Point[0]));
        System.out.println("maxPoints = " + maxPoints);

    }

    @Test
    public void test3() throws Exception {
        MaxPointsOnALine_Version2 maxPointsOnALine = new MaxPointsOnALine_Version2();
        ArrayList<Point> points = new ArrayList<Point>();
        points.add(new Point(1, 1));
        points.add(new Point(1, 1));
        points.add(new Point(2, 3));

        int maxPoints = maxPointsOnALine.maxPoints(points.toArray(new Point[0]));
        System.out.println("maxPoints = " + maxPoints);

    }
}
