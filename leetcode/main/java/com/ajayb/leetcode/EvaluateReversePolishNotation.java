package com.ajayb.leetcode;

import java.util.LinkedList;

/**
 * To change this template use File | Settings | File Templates.
 */
public class EvaluateReversePolishNotation {

    public static final String ADDITION = "+";
    public static final String SUBTRACTION = "-";
    public static final String MULTIPLICATION = "*";
    public static final String DIVISION = "/";

    private String[] operators = {ADDITION, SUBTRACTION, MULTIPLICATION, DIVISION};

    public int evalRPN(String[] tokens) {
        LinkedList<Integer> deque = new LinkedList<Integer>();
        for (String token : tokens) {
            String operator = null;
            for (int i = 0; i < operators.length; i++) {

                if (token.trim().equals(operators[i])) {
                    operator = operators[i];
                }
            }

            if (operator != null) {
                Integer secondOperand = deque.removeLast();
                Integer firstOperand = deque.removeLast();

                Integer intermediateResult = 0;
                if (operator.equals(ADDITION)) {
                    intermediateResult = firstOperand + secondOperand;
                } else if (operator.equals(SUBTRACTION)) {
                    intermediateResult = firstOperand - secondOperand;
                } else if (operator.equals(MULTIPLICATION)) {
                    intermediateResult = firstOperand * secondOperand;
                } else if (operator.equals(DIVISION)) {
                    intermediateResult = firstOperand / secondOperand;
                }

                deque.addLast(intermediateResult);
            } else {
                deque.addLast(new Integer(token));
            }

        }
        return deque.removeLast();
    }
}
