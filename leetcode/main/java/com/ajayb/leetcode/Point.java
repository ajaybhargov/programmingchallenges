package com.ajayb.leetcode;

/**
 * To change this template use File | Settings | File Templates.
 */
class Point {
    int x;
    int y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Point() {
        x = 0;
        y = 0;
    }

}
