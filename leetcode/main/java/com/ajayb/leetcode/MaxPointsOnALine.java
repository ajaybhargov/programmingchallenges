package com.ajayb.leetcode;

import java.util.HashMap;

/**
 * To change this template use File | Settings | File Templates.
 */
public class MaxPointsOnALine {
    public int maxPoints(Point[] points) {
        HashMap<Integer, Integer> pointsFallingOnTheSameHorizontalLine = new HashMap<Integer, Integer>();
        HashMap<Integer, Integer> pointsFallingOnTheSameVerticalLine = new HashMap<Integer, Integer>();
        Integer pointsFallingOnTheSameDiagonalLine = 0;

        for (int i = 0; i < points.length; i++) {
            int x = points[i].x;
            Integer count = 0;
            if (pointsFallingOnTheSameHorizontalLine.containsKey(x)) {
                count = pointsFallingOnTheSameHorizontalLine.get(x);
            }
            pointsFallingOnTheSameHorizontalLine.put(x, ++count);

            int y = points[i].y;
            Integer verticalCount = 0;
            if (pointsFallingOnTheSameVerticalLine.containsKey(y)) {
                verticalCount = pointsFallingOnTheSameVerticalLine.get(y);
            }
            pointsFallingOnTheSameVerticalLine.put(y, ++verticalCount);

            if (x == y) {
                ++pointsFallingOnTheSameDiagonalLine;
            }

        }

        int max = 0;
        for (Integer key : pointsFallingOnTheSameHorizontalLine.keySet()) {
            if (pointsFallingOnTheSameHorizontalLine.get(key) > max) {
                max = pointsFallingOnTheSameHorizontalLine.get(key);
            }
        }

        for (Integer key : pointsFallingOnTheSameVerticalLine.keySet()) {
            if (pointsFallingOnTheSameVerticalLine.get(key) > max) {
                max = pointsFallingOnTheSameVerticalLine.get(key);
            }
        }

        if (pointsFallingOnTheSameDiagonalLine > max) {
            max = pointsFallingOnTheSameDiagonalLine;
        }


        return max;

    }

}

