package com.ajayb.leetcode;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * To change this template use File | Settings | File Templates.
 */
public class MaxPointsOnALine_Version2 {
    public int maxPoints(Point[] points) {
        int maxLine = 0;

        for (int i = 0; i < (points.length - maxLine); i++) {
            int coincident = 0;
            Map<Double, Integer> pointCounts = new HashMap<Double, Integer>();
            for (int j = i + 1; j < points.length; j++) {
                Double slope;
                if (points[i].x == points[j].x && points[i].y == points[j].y) {
                    coincident++;
                    continue;
                } else if (points[i].x == points[j].x) {
                    slope = Math.PI;
                } else if (points[i].y == points[j].y) {
                    slope = 0.0; // logically we don't need this, but in practice i find that we do
                } else {
                    slope = new Double((double) (points[i].y - points[j].y) / (double) (points[i].x - points[j].x));
                }

                if (pointCounts.containsKey(slope))
                    pointCounts.put(slope, pointCounts.get(slope) + 1);
                else
                    pointCounts.put(slope, new Integer(1));
            }
            maxLine = Math.max(maxLine, 1 + coincident + maxValue(pointCounts));
        }

        return maxLine;
    }

    private int maxValue(Map<Double, Integer> doubleIntMap) {
        int max = 0;
        Set<Double> keys = doubleIntMap.keySet();
        Iterator iter = keys.iterator();
        while (iter.hasNext())
            max = Math.max(max, doubleIntMap.get(iter.next()));
        return max;
    }
}