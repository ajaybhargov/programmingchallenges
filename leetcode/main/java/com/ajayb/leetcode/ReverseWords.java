package com.ajayb.leetcode;

/**
 * To change this template use File | Settings | File Templates.
 */
public class ReverseWords {
    public String reverseWords(String s) {
        s = s.trim();
        String[] splits = s.split("\\s+");
        StringBuilder finalStringToBeConstructed = new StringBuilder();
        for(int i =splits.length-1;i>=0;i--){
            finalStringToBeConstructed.append(splits[i]);
            if(i>0) {
                finalStringToBeConstructed.append(" ");
            }
        }
        return finalStringToBeConstructed.toString();
    }
}
