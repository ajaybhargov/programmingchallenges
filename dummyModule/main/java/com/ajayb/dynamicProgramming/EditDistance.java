package com.ajayb.dynamicProgramming;

/**
 * To change this template use File | Settings | File Templates.
 */
public class EditDistance {

    private enum COST {
        MATCH(0), INSERTION(1), DELETION(1);

        private int cost;

        COST(int cost) {
            this.cost = cost;
        }
    }

    private String text;
    private String stringToBeSearched;
    private Integer[][] costArray;
    private Integer[][] parentArray;
    private Integer[] operation;

    public EditDistance(String text, String stringToBeSearched) {
        this.text = text;
        this.stringToBeSearched = stringToBeSearched;
        costArray = new Integer[stringToBeSearched.length()][text.length()];
        parentArray = new Integer[stringToBeSearched.length()][text.length()];
        operation = new Integer[3];
    }

    public void search() {
        initialize();

        for (int i = 1; i <= stringToBeSearched.length(); i++) {
            for (int j = 1; j <= text.length(); j++) {
                //
            }
        }
    }

    private void initialize() {
        initializeFirstRow();
        initializeFirstColumn();
    }

    private void initializeFirstColumn() {
        for (int i = 0; i <= stringToBeSearched.length(); i++) {
            costArray[i][0] = 0;
        }
    }

    private void initializeFirstRow() {
        for (int j = 0; j <= text.length(); j++) {
            costArray[0][j] = 0;
        }
    }


}
